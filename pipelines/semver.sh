#!/usr/bin/env bash
#source os # This gives the script access to GitLab environment variables, supposedly...

# This function attempts to get the tags from the .git, 
# if there are no tags it will set the tag to 0.1.0.
function get_tags() { 
    tag=$(git describe --tags 2>/dev/null) || tag="0.1.0" 
}

get_tags
printf "The tag for this repo is: %s\n" $tag

# This function gets out existing tags from get_tags then,
# converts it to the correct SemVer format, from vx.x to x.x.x.
function convert_tag() {
    printf "Converting tag from: %s " $tag
    
    tag=$(echo $tag | sed 's/[^0-9.].*//g')
    
    printf "Converted to: %s\n" "$tag"
    
    bump_minor
}

function bump_minor() {
    str=$tag
    re='^([0-9][.][0-9][.][0-9])$'
    if [[ $str =~ $re ]]; then
#      echo "Match!"
#      var=$(echo $semver | sed 's/[^0-9]*//g')
#      echo $var
      
      IFS=. read -ra a <<<"$str"                # Splits semver by . and stores in array a using IFS (input field separator)
      ((a[2]++))                                # Increments minor version from x.x.x to x.x.(x+1)
      semver="${a[0]}.${a[1]}.${a[2]}"
      printf "New Version: %s\n" "$semver"
    else
      convert_tag
    fi  
}

bump_minor

function tag_repo() {
    url=$CI_REPOSITORY_URL
    #echo "GitLab CI_REPOSITORY_URL: $posurl" 
    #url=$(git config --get remote.origin.url)

     push_url=$(echo "$url" | sed -r 's/.*@//' | sed -r 's/^/git@/' | sed -r 's@/@:@')
#    push_url=$(echo "$url" | sed -r 's/[][]//g' | sed -r 's/MASKED//g' | sed -r 's@https://gitlab-ci-token:@git@' | sed -r 's@/@:@')
    
#    push_url=$(echo "$url" | sed -r 's#(http.*://)([^/]+)/(.+)$#git@\2:\3#g') # 's#(http.*://)([^/]+)/(.+)$#git@\2:\3.git#g' if the url has no .git at the end.
    echo "Converted SSH URL: $push_url"
    
#    API_JSON=$(printf '{"tag_name": "v%s","target_commitish": "master","name": "v%s","body": "Release of version %s","draft": false,"prerelease": false}' $VERSION $VERSION $VERSION)
#    curl --data "$API_JSON" ${push_url}  # This is for creating a release instead of a tag. TODO --> Complete
    
    git config --global user.email "jasonmorsley@hotmail.co.uk"
    git config --global user.name "Jason Morsley"


    git remote set-url --push origin "${push_url}"
    git tag -a ${semver} -m "Release of version: ${semver}"
    git push origin ${semver}

    echo "end of script reached."
}

tag_repo
